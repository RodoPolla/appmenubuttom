package com.example.appmenubuttom92

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AcercaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AcercaFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var rcvLista:RecyclerView
    private lateinit var adaptador:MiAdaptador
    private lateinit var alumno:AlumnoLista

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val View=inflater.inflate(R.layout.fragment_acerca,container,false)
        // Inflate the layout for this fragment
        rcvLista=View.findViewById(R.id.recId)
        rcvLista.layoutManager=LinearLayoutManager(requireContext())
        val listaAlumno=ArrayList<AlumnoLista>()
        listaAlumno.add(AlumnoLista(1,"2021030111","Mendoza Lizarraga Rodolfo","","Ing. Tec. Informacion",R.mipmap.alumno))
        listaAlumno.add(AlumnoLista(2,"2021030222","Mejia Vazquez David","","Ing. Tec. Informacion",R.mipmap.a01))
        listaAlumno.add(AlumnoLista(3,"2021030333","Quintero Herrera Irving","","Ing. Tec. Informacion",R.mipmap.a03))
        adaptador= MiAdaptador(listaAlumno,requireContext())
        rcvLista.adapter=adaptador
        return View
        //return inflater.inflate(R.layout.fragment_acerca, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AcercaFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AcercaFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}