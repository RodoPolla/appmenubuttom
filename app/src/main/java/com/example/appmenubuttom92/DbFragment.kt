package com.example.appmenubuttom92

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.appmenubuttom92.DataBase.Alumno
import com.example.appmenubuttom92.DataBase.dbAlumnos

class DbFragment : Fragment() {
    private lateinit var btnAgregar: Button
    private lateinit var btnConsultar: Button
    private lateinit var btnBorrar: Button
    private lateinit var etNombre: EditText
    private lateinit var etMatricula: EditText
    private lateinit var etDomicilio: EditText
    private lateinit var etEspecialidad: EditText
    private lateinit var db: dbAlumnos
    private var currentAlumnoId: Int = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        // Initialize UI elements
        btnAgregar = view.findViewById(R.id.btnAgregar)
        btnConsultar = view.findViewById(R.id.btnConsultar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        etNombre = view.findViewById(R.id.etNombre)
        etMatricula = view.findViewById(R.id.etMatricula)
        etDomicilio = view.findViewById(R.id.etDomicilio)
        etEspecialidad = view.findViewById(R.id.etEspecialidad)

        db = dbAlumnos(requireContext())
        db.openDatabase()

        // Set up button click listeners
        btnAgregar.setOnClickListener {
            val matricula = etMatricula.text.toString()
            val nombre = etNombre.text.toString()
            val domicilio = etDomicilio.text.toString()
            val especialidad = etEspecialidad.text.toString()

            if (matricula.isEmpty() || nombre.isEmpty() || domicilio.isEmpty() || especialidad.isEmpty()) {
                Toast.makeText(requireContext(), "Por favor, llena todos los campos obligatorios", Toast.LENGTH_SHORT).show()
            } else {
                val existingAlumno = db.getAlumnoPorMatricula(matricula)
                if (existingAlumno != null) {
                    // Actualizar el alumno existente
                    val alumno = Alumno(
                        id = existingAlumno.id,
                        nombre = nombre,
                        matricula = matricula,
                        domicilio = domicilio,
                        especialidad = especialidad,
                        foto = "" // Campo no obligatorio, se deja vacío
                    )
                    val rowsUpdated = db.actualizarAlumno(alumno, existingAlumno.id)
                    if (rowsUpdated > 0) {
                        Toast.makeText(requireContext(), "Alumno actualizado con éxito", Toast.LENGTH_SHORT).show()
                        limpiarCampos()
                    } else {
                        Toast.makeText(requireContext(), "Error al actualizar alumno", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    // Insertar nuevo alumno
                    val alumno = Alumno(
                        nombre = nombre,
                        matricula = matricula,
                        domicilio = domicilio,
                        especialidad = especialidad,
                        foto = "" // Campo no obligatorio, se deja vacío
                    )
                    val id = db.insertarAlumno(alumno)
                    if (id > 0) {
                        Toast.makeText(requireContext(), "Alumno agregado con éxito", Toast.LENGTH_SHORT).show()
                        limpiarCampos()
                    } else {
                        Toast.makeText(requireContext(), "Error al agregar alumno", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        btnConsultar.setOnClickListener {
            val matricula = etMatricula.text.toString()
            val alumno = db.getAlumnoPorMatricula(matricula)
            if (alumno != null) {
                currentAlumnoId = alumno.id
                etNombre.setText(alumno.nombre)
                etDomicilio.setText(alumno.domicilio)
                etEspecialidad.setText(alumno.especialidad)
                Toast.makeText(requireContext(), "Alumno consultado con éxito", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), "Alumno no encontrado", Toast.LENGTH_SHORT).show()
            }
        }

        btnBorrar.setOnClickListener {
            val matricula = etMatricula.text.toString()
            val alumno = db.getAlumnoPorMatricula(matricula)
            if (alumno != null) {
                AlertDialog.Builder(requireContext())
                    .setTitle("Confirmación")
                    .setMessage("¿Deseas borrar el alumno con matrícula $matricula?")
                    .setPositiveButton("Sí") { dialog, which ->
                        val rowsDeleted = db.borrarAlumno(alumno.id)
                        if (rowsDeleted > 0) {
                            Toast.makeText(requireContext(), "Alumno borrado con éxito", Toast.LENGTH_SHORT).show()
                            limpiarCampos()
                        } else {
                            Toast.makeText(requireContext(), "Error al borrar alumno", Toast.LENGTH_SHORT).show()
                        }
                    }
                    .setNegativeButton("No") { dialog, which ->
                        dialog.dismiss()
                    }
                    .show()
            } else {
                Toast.makeText(requireContext(), "Alumno no encontrado", Toast.LENGTH_SHORT).show()
            }
        }

        return view
    }

    private fun limpiarCampos() {
        etNombre.text.clear()
        etMatricula.text.clear()
        etDomicilio.text.clear()
        etEspecialidad.text.clear()
        currentAlumnoId = -1
    }
}