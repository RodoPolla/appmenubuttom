package com.example.appmenubuttom92

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnKeyListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder

class MiAdaptador (
    protected val listaAlumnos:ArrayList<com.example.appmenubuttom92.AlumnoLista>,private val context:Context):RecyclerView.Adapter<MiAdaptador.ViewHolder>(),View.OnKeyListener{
    private val inflater:LayoutInflater=LayoutInflater.from(context)
    private var listener:View.OnKeyListener?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiAdaptador.ViewHolder {
        val view=inflater.inflate(R.layout.alumn_item,parent,false)
        view.setOnClickListener(this)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: MiAdaptador.ViewHolder, position: Int) {
        val alumno=listaAlumnos[position]
        holder.txtMatricula.setText(alumno.matricula)
        holder.txtNombre.setText(alumno.nombre)
        holder.idImagen.setImageResource(alumno.foto)
        holder.txtCarrera.setText(alumno.especialidad)
    }

    override fun getItemCount(): Int {
        return listaAlumnos.size
    }

    fun setOnClickListener(listener: OnKeyListener){
        this.listener=listener
    }

    override fun onClick(v:View){
        listener?.onClick(v)
    }

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val txtNombre: TextView =itemView.findViewById<TextView>(R.id.txtAlumnoNombre)
        val txtMatricula: TextView =itemView.findViewById<TextView>(R.id.txtMatricula)
        val txtCarrera: TextView =itemView.findViewById<TextView>(R.id.txtCarrera)
        val idImagen: ImageView =itemView.findViewById<ImageView>(R.id.foto)
    }

}
