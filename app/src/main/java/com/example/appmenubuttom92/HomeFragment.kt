package com.example.appmenubuttom92

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<ImageView>(R.id.whatsapp_icon).setOnClickListener { onIconClick(it) }
        view.findViewById<ImageView>(R.id.facebook_icon).setOnClickListener { onIconClick(it) }
        view.findViewById<ImageView>(R.id.instagram_icon).setOnClickListener { onIconClick(it) }
        view.findViewById<ImageView>(R.id.linkedin_icon).setOnClickListener { onIconClick(it) }
    }

    fun onIconClick(view: View) {
        val url = when (view.id) {
            R.id.whatsapp_icon -> "https://wa.me/yourphonenumber"
            R.id.facebook_icon -> "https://www.facebook.com/yourprofile"
            R.id.instagram_icon -> "https://www.instagram.com/yourprofile"
            R.id.linkedin_icon -> "https://www.linkedin.com/in/yourprofile"
            else -> ""
        }
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        try {
            context?.let {
                startActivity(intent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(context, "No se pudo abrir el enlace", Toast.LENGTH_SHORT).show()
        }
    }
}